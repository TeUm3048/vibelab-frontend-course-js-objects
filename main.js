"use strict";

const numberOfFilms = prompt("Сколько фильмов вы уже посмотрели?");

const personalMovieDB = {
  count: numberOfFilms,
  movies: {},
  actors: {},
  genres: [],
  privat: false,

  /* The `showMyDB` function is a method of the `personalMovieDB` object. It is used to check if the
  `privat` property of the `personalMovieDB` object is set to `false`. If it is `false`, the
  function returns the `personalMovieDB` object itself. If it is `true`, the function returns
  `null`. This function is used to determine whether the database should be displayed or not. */
  showMyDB: function () {
    if (!this?.privat) {
      return this;
    }
    return null;
  },

  /* The `writeYourGenres` method is a function that prompts the user to enter their favorite movie
  genres. It takes an optional parameter `genreCount` which defaults to 3. */
  writeYourGenres: function (genreCount = 3) {
    for (let i = 1; i <= genreCount; i++) {
      let genre = prompt(`Ваш любимый жанр под номером ${i}`);

      /* The `while` loop is used to validate the user input for the genre name. */
      while (!genre?.length > 0) {
        genre = prompt(`Ваш любимый жанр под номером ${i}`);
      }

      this.genres.push(genre);
    }

    this.genres.forEach((genre, ind) => {
      console.log(`Любимый жанр #${ind + 1} - это ${genre}`);
    });
  },

  /**
   * The method `validateMovie` checks if a movie is valid based on its length and if it already exists
   * in a database.
   * @param {string | null} movie - The `movie` parameter is a string that represents the name of a movie.
   * @returns {boolean} The function `validateMovie` returns `true` if the `movie` meets all the validation
   * criteria, and `false` otherwise.
   */
  validateMovie: function (movie) {
    return movie.length > 0 && movie.length <= 50 && !(movie in this.movies);
  },

  /* The `toggleVisibleMyDB` method is a function that toggles the value of the `privat` property of
  the `personalMovieDB` object. */
  toggleVisibleMyDB: function () {
    this.privat = !this.privat;
    return;
  },
};

personalMovieDB.writeYourGenres();

/* The `for` loop is used to prompt the user for the name and score of each movie they have watched. */
for (let i = 0; i < numberOfFilms; i++) {
  let movie = prompt("Один из последних просмотренных фильмов?");
  let movieScore = prompt("На сколько оцените его?");

  /* The `while` loop is used to validate the user input for the movie name. */
  while (!personalMovieDB.validateMovie(movie)) {
    movie = prompt(
      "Имя не должно быть пустым, длиннее 50 символов и повторятся.\n\
       Введите один из последних просмотренных фильмов"
    );
    movieScore = prompt("На сколько оцените его?");
  }

  /* Add a new key-value pair `movie: movieScore` to the
  `movies` property of the `personalMovieDB` object. */
  personalMovieDB.movies[movie] = movieScore;
}

console.log(personalMovieDB);
